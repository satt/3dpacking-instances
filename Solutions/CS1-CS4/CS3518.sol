%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instance: CS3518
% Mon Mar  1 18:39:56 2010
% Syntax:
% NumUsedContainers
% IdContainerType (length,width,height) NumBoxes
% IdBox (length,width,height) container [x, y, z]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

2

ct0 ( 240,1350, 245) 65
bt18 ( 161, 126, 137) [     0,    0,    0]
bt15 ( 108, 118, 100) [     0,    0,  137]
bt12 (  69,  68,  61) [   161,    0,    0]
 bt0 (  25,  10,   3) [   161,    0,   61]
bt18 ( 161, 126, 137) [     0,  126,    0]
bt15 ( 108, 118, 100) [     0,  126,  137]
bt12 (  69,  68,  61) [   161,   68,    0]
bt12 (  69,  68,  61) [   161,  136,    0]
bt12 (  68,  69,  61) [   161,   68,   61]
bt12 (  68,  69,  61) [   161,   68,  122]
bt12 (  68,  69,  61) [   161,   68,  183]
 bt0 (  10,  25,   3) [   230,    0,    0]
bt18 ( 161, 126, 137) [     0,  252,    0]
bt15 ( 118, 108, 100) [     0,  252,  137]
 bt0 (  10,  25,   3) [   161,  204,    0]
 bt0 (  10,  25,   3) [   161,  229,    0]
bt19 ( 154, 142, 179) [     0,  378,    0]
bt12 (  69,  68,  61) [     0,  378,  179]
bt12 (  69,  68,  61) [    69,  378,  179]
bt12 (  69,  68,  61) [     0,  446,  179]
bt11 (  69,  61,  68) [   154,  378,    0]
bt11 (  69,  61,  68) [   154,  439,    0]
bt19 ( 179, 142, 154) [     0,  520,    0]
bt12 (  69,  68,  61) [     0,  520,  154]
 bt6 (  45,  46,  41) [   179,  500,    0]
 bt6 (  45,  46,  41) [   179,  546,    0]
bt18 ( 161, 126, 137) [     0,  662,    0]
bt15 ( 108, 118, 100) [     0,  662,  137]
bt11 (  61,  69,  68) [   161,  662,    0]
bt19 ( 179, 142, 154) [     0,  788,    0]
 bt7 (  50,  49,  44) [     0,  788,  154]
 bt7 (  50,  49,  44) [    50,  788,  154]
 bt2 (  26,  33,  21) [   179,  731,    0]
bt19 ( 179, 154, 142) [     0,  930,    0]
bt15 ( 118, 108, 100) [     0,  930,  142]
bt11 (  61,  69,  68) [   179,  764,    0]
bt19 ( 179, 142, 154) [     0, 1084,    0]
 bt7 (  49,  50,  44) [     0, 1084,  154]
 bt7 (  49,  50,  44) [    49, 1084,  154]
 bt5 (  49,  50,  44) [     0, 1084,  198]
 bt5 (  49,  50,  44) [    49, 1084,  198]
 bt8 (  61,  54,  60) [   179,  833,    0]
 bt8 (  61,  54,  60) [   179,  887,    0]
 bt8 (  61,  54,  60) [   179,  941,    0]
 bt8 (  61,  54,  60) [   179,  995,    0]
 bt8 (  61,  54,  60) [   179, 1049,    0]
 bt8 (  61,  54,  60) [   179, 1103,    0]
bt10 (  61,  60,  54) [   179,  833,   60]
bt10 (  61,  60,  54) [   179,  893,   60]
bt10 (  61,  60,  54) [   179,  953,   60]
bt10 (  61,  60,  54) [   179, 1013,   60]
bt10 (  61,  60,  54) [   179, 1073,   60]
bt10 (  61,  60,  54) [   179,  833,  114]
bt10 (  61,  60,  54) [   179,  893,  114]
bt10 (  61,  60,  54) [   179,  953,  114]
bt10 (  61,  60,  54) [   179, 1013,  114]
bt10 (  61,  60,  54) [   179, 1073,  114]
 bt3 (  55,  33,  21) [   179,  833,  168]
bt16 ( 140, 112, 110) [     0, 1226,    0]
bt13 (  74,  77,  67) [     0, 1226,  110]
 bt4 (  41,  36,  43) [     0, 1226,  177]
 bt1 (  31,  10,   3) [     0, 1226,  220]
 bt1 (  31,  10,   3) [     0, 1236,  220]
 bt1 (  31,  10,   3) [     0, 1246,  220]
 bt1 (  31,  10,   3) [     0, 1226,  223]

ct0 ( 240,1350, 245) 28
bt17 ( 126, 113, 144) [     0,    0,    0]
 bt9 (  55,  49,  55) [     0,    0,  144]
 bt9 (  55,  49,  55) [    55,    0,  144]
 bt9 (  55,  49,  55) [     0,   49,  144]
 bt9 (  55,  49,  55) [    55,   49,  144]
 bt4 (  43,  36,  41) [     0,    0,  199]
bt13 (  74,  77,  67) [   126,    0,    0]
bt13 (  74,  77,  67) [   126,    0,   67]
 bt9 (  55,  55,  49) [   126,    0,  134]
 bt9 (  49,  55,  55) [   126,    0,  183]
bt17 ( 144, 126, 113) [     0,  113,    0]
bt17 ( 144, 126, 113) [     0,  113,  113]
bt14 (  85,  86,  75) [   144,   77,    0]
bt14 (  85,  86,  75) [   144,   77,   75]
bt14 (  85,  86,  75) [   144,   77,  150]
bt17 ( 144, 126, 113) [     0,  239,    0]
bt17 ( 144, 113, 126) [     0,  239,  113]
bt14 (  85,  86,  75) [   144,  163,    0]
bt14 (  85,  86,  75) [   144,  249,    0]
bt13 (  74,  77,  67) [   144,  163,   75]
bt13 (  74,  77,  67) [   144,  240,   75]
 bt9 (  55,  55,  49) [   144,  163,  142]
 bt9 (  55,  55,  49) [   144,  218,  142]
 bt9 (  55,  55,  49) [   144,  163,  191]
 bt9 (  55,  55,  49) [   144,  218,  191]
bt17 ( 144, 126, 113) [     0,  365,    0]
bt17 ( 144, 126, 113) [     0,  365,  113]
 bt1 (  31,  10,   3) [     0,  365,  226]


Cost = 267430182