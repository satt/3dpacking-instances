%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instance: CS4970
% Mon Mar  1 18:39:43 2010
% Syntax:
% NumUsedContainers
% IdContainerType (length,width,height) NumBoxes
% IdBox (length,width,height) container [x, y, z]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

2

2 ( 240,1340, 240) 23
bt11 ( 106, 101,  92) [     0,    0,    0]
 bt7 (  67,  77,  74) [     0,    0,   92]
 bt1 (  41,  43,  36) [     0,    0,  166]
bt10 ( 106,  92, 101) [   106,    0,    0]
 bt4 (  55,  55,  49) [   106,    0,  101]
 bt3 (  49,  50,  44) [   106,    0,  150]
bt12 ( 154, 142, 179) [     0,  101,    0]
bt12 ( 154, 142, 179) [     0,  243,    0]
 bt2 (  46,  45,  41) [     0,  243,  179]
 bt5 (  61,  60,  54) [   154,   92,    0]
bt12 ( 154, 142, 179) [     0,  385,    0]
bt12 ( 154, 142, 179) [     0,  527,    0]
 bt9 (  86,  85,  75) [   154,  152,    0]
bt12 ( 154, 142, 179) [     0,  669,    0]
 bt1 (  41,  43,  36) [     0,  669,  179]
bt12 ( 154, 142, 179) [     0,  811,    0]
 bt2 (  46,  45,  41) [     0,  811,  179]
 bt8 (  75,  86,  85) [   154,  237,    0]
bt12 ( 154, 142, 179) [     0,  953,    0]
 bt6 (  69,  61,  68) [   154,  323,    0]
 bt0 (  43,  41,  36) [   154,  323,   68]
bt12 ( 142, 154, 179) [     0, 1095,    0]
 bt0 (  43,  41,  36) [     0, 1095,  179]

2 ( 240,1340, 240) 3
bt12 ( 154, 142, 179) [     0,    0,    0]
bt12 ( 154, 142, 179) [     0,  142,    0]
 bt2 (  46,  45,  41) [     0,  142,  179]


Cost = 140827089