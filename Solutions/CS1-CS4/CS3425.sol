%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instance: CS3425
% Mon Mar  1 18:39:50 2010
% Syntax:
% NumUsedContainers
% IdContainerType (length,width,height) NumBoxes
% IdBox (length,width,height) container [x, y, z]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

2

ct0 ( 240,1350, 245) 56
 bt2 (  69,  61,  68) [     0,    0,    0]
 bt1 (  60,  61,  54) [     0,    0,   68]
 bt2 (  69,  61,  68) [    69,    0,    0]
 bt7 (  95,  82,  92) [     0,   61,    0]
 bt7 (  95,  82,  92) [     0,   61,   92]
 bt4 (  69,  68,  61) [     0,   61,  184]
 bt2 (  69,  61,  68) [    95,   61,    0]
 bt2 (  69,  61,  68) [   164,    0,    0]
 bt2 (  69,  61,  68) [   164,   61,    0]
 bt4 (  69,  68,  61) [   164,    0,   68]
bt10 ( 100, 129, 116) [     0,  143,    0]
bt10 ( 100, 129, 116) [     0,  143,  116]
bt10 ( 129, 100, 116) [   100,  122,    0]
bt10 ( 116, 100, 129) [   100,  122,  116]
bt10 ( 100, 129, 116) [     0,  272,    0]
bt10 ( 100, 116, 129) [     0,  272,  116]
bt10 ( 129, 100, 116) [   100,  222,    0]
bt10 ( 129, 100, 116) [   100,  222,  116]
bt10 ( 100, 129, 116) [     0,  401,    0]
bt10 ( 100, 116, 129) [     0,  401,  116]
bt10 ( 129, 100, 116) [   100,  322,    0]
bt10 ( 129, 100, 116) [   100,  422,    0]
bt10 ( 116, 100, 129) [   100,  322,  116]
bt10 ( 116, 100, 129) [   100,  422,  116]
bt10 ( 100, 116, 129) [     0,  530,    0]
 bt6 (  75,  86,  85) [     0,  530,  129]
bt10 ( 129, 116, 100) [   100,  522,    0]
bt15 ( 126, 113, 144) [   100,  522,  100]
 bt3 (  77,  74,  67) [     0,  646,    0]
 bt3 (  74,  67,  77) [     0,  646,   67]
 bt3 (  74,  67,  77) [     0,  646,  144]
 bt3 (  74,  67,  77) [    77,  646,    0]
 bt3 (  74,  67,  77) [    77,  646,   77]
 bt5 (  77,  74,  67) [   151,  638,    0]
 bt3 (  77,  74,  67) [   151,  638,   67]
 bt3 (  77,  67,  74) [   151,  638,  134]
 bt3 (  74,  67,  77) [     0,  720,    0]
 bt3 (  74,  67,  77) [     0,  720,   77]
 bt0 (  61,  60,  54) [     0,  720,  154]
bt13 ( 101, 116, 197) [     0,  787,    0]
bt10 ( 129, 100, 116) [   101,  713,    0]
bt10 ( 129, 100, 116) [   101,  713,  116]
bt15 ( 113, 126, 144) [     0,  903,    0]
 bt9 ( 106,  92, 101) [     0,  903,  144]
bt15 ( 126, 113, 144) [   113,  813,    0]
 bt9 (  92, 106, 101) [   113,  813,  144]
bt15 ( 113, 126, 144) [     0, 1029,    0]
 bt9 (  92, 106, 101) [     0, 1029,  144]
bt15 ( 126, 113, 144) [   113,  926,    0]
bt15 ( 126, 113, 144) [   113, 1039,    0]
 bt9 (  92, 106, 101) [   113,  926,  144]
 bt9 (  92, 106, 101) [   113, 1032,  144]
bt15 ( 113, 126, 144) [     0, 1155,    0]
 bt5 (  74,  77,  67) [     0, 1155,  144]
bt15 ( 113, 126, 144) [   113, 1152,    0]
bt12 ( 101, 106,  92) [   113, 1152,  144]

ct1 ( 240,1340, 240) 14
bt11 ( 118, 100, 108) [     0,    0,    0]
 bt8 (  85,  86,  75) [     0,    0,  108]
bt11 ( 118, 100, 108) [   118,    0,    0]
bt11 ( 118, 100, 108) [   118,    0,  108]
bt16 ( 137, 126, 161) [     0,  100,    0]
 bt8 (  86,  85,  75) [     0,  100,  161]
bt11 ( 100, 118, 108) [   137,  100,    0]
bt11 ( 100, 118, 108) [   137,  100,  108]
bt16 ( 137, 126, 161) [     0,  226,    0]
 bt8 (  85,  86,  75) [     0,  226,  161]
bt14 ( 100, 118, 108) [   137,  218,    0]
bt14 ( 100, 118, 108) [   137,  218,  108]
bt16 ( 126, 137, 161) [     0,  352,    0]
 bt8 (  86,  85,  75) [     0,  352,  161]


Cost = 274566803