%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instance: CS3636
% Mon Mar  1 18:39:52 2010
% Syntax:
% NumUsedContainers
% IdContainerType (length,width,height) NumBoxes
% IdBox (length,width,height) container [x, y, z]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

2

ct0 ( 240,1350, 245) 3
bt19 ( 149, 148, 154) [     0,    0,    0]
bt20 ( 155, 168, 195) [     0,  148,    0]
 bt0 (  42,  33,  38) [     0,  148,  195]

ct0 ( 240,1350, 245) 86
 bt1 (  40,  37,  33) [     0,    0,    0]
 bt1 (  40,  37,  33) [     0,    0,   33]
 bt1 (  40,  37,  33) [     0,    0,   66]
 bt1 (  40,  37,  33) [     0,    0,   99]
 bt1 (  40,  37,  33) [     0,    0,  132]
 bt1 (  40,  37,  33) [     0,    0,  165]
 bt1 (  40,  37,  33) [     0,    0,  198]
 bt1 (  40,  37,  33) [    40,    0,    0]
 bt1 (  40,  37,  33) [    40,    0,   33]
 bt1 (  40,  37,  33) [    40,    0,   66]
bt13 ( 106,  92, 101) [     0,   37,    0]
bt13 ( 106,  92, 101) [   106,    0,    0]
bt13 ( 106,  92, 101) [     0,  129,    0]
 bt7 (  64,  68,  57) [   106,   92,    0]
 bt7 (  64,  68,  57) [   106,   92,   57]
 bt7 (  64,  68,  57) [   106,   92,  114]
 bt5 (  55,  58,  49) [   106,   92,  171]
 bt6 (  57, 153,  75) [     0,  221,    0]
 bt5 (  55,  58,  49) [     0,  221,   75]
 bt5 (  55,  58,  49) [     0,  279,   75]
 bt5 (  55,  58,  49) [    57,  221,    0]
 bt5 (  55,  58,  49) [    57,  279,    0]
 bt1 (  40,  37,  33) [    57,  221,   49]
 bt1 (  40,  37,  33) [    57,  258,   49]
 bt7 (  68,  64,  57) [   112,  160,    0]
 bt7 (  68,  64,  57) [   112,  224,    0]
 bt7 (  68,  64,  57) [   112,  288,    0]
 bt7 (  68,  64,  57) [   112,  160,   57]
 bt7 (  68,  64,  57) [   112,  224,   57]
 bt7 (  68,  64,  57) [   112,  288,   57]
 bt7 (  68,  64,  57) [   112,  160,  114]
 bt7 (  68,  64,  57) [   112,  224,  114]
 bt7 (  68,  64,  57) [   112,  288,  114]
 bt5 (  58,  55,  49) [   112,  160,  171]
 bt5 (  58,  55,  49) [   112,  215,  171]
 bt5 (  58,  55,  49) [   112,  270,  171]
 bt5 (  58,  55,  49) [   180,   92,    0]
 bt5 (  58,  55,  49) [   180,  147,    0]
 bt5 (  58,  55,  49) [   180,  202,    0]
 bt5 (  58,  55,  49) [   180,  257,    0]
 bt5 (  58,  55,  49) [   180,  312,    0]
 bt5 (  58,  55,  49) [     0,  374,    0]
 bt1 (  40,  37,  33) [     0,  374,   49]
bt10 (  86,  75,  85) [     0,  429,    0]
bt10 (  86,  75,  85) [     0,  429,   85]
bt10 (  86,  75,  85) [    86,  352,    0]
bt10 (  86,  75,  85) [    86,  427,    0]
bt10 (  86,  75,  85) [    86,  352,   85]
bt10 (  86,  75,  85) [    86,  427,   85]
 bt5 (  55,  58,  49) [    86,  352,  170]
 bt5 (  55,  58,  49) [    86,  410,  170]
 bt5 (  58,  55,  49) [   172,  367,    0]
 bt5 (  58,  55,  49) [   172,  422,    0]
bt13 ( 106,  92, 101) [     0,  504,    0]
bt10 (  75,  86,  85) [   106,  502,    0]
bt10 (  75,  86,  85) [   106,  502,   85]
 bt5 (  55,  58,  49) [   106,  502,  170]
 bt5 (  55,  58,  49) [   181,  477,    0]
 bt5 (  55,  58,  49) [   181,  535,    0]
bt18 ( 113, 126, 144) [     0,  596,    0]
 bt4 (  61,  54,  60) [     0,  596,  144]
 bt4 (  61,  54,  60) [     0,  650,  144]
bt16 ( 116, 101, 197) [   113,  593,    0]
 bt2 (  42,  40,  38) [   113,  593,  197]
bt18 ( 113, 126, 144) [     0,  722,    0]
bt11 (  79,  77,  69) [     0,  722,  144]
bt18 ( 126, 113, 144) [   113,  694,    0]
bt17 ( 118, 108, 100) [   113,  694,  144]
bt11 (  79,  77,  69) [     0,  848,    0]
bt11 (  79,  77,  69) [     0,  848,   69]
bt11 (  79,  77,  69) [     0,  848,  138]
 bt2 (  40,  42,  38) [     0,  848,  207]
bt18 ( 113, 126, 144) [     0,  925,    0]
 bt8 (  77,  67,  74) [     0,  925,  144]
bt18 ( 126, 113, 144) [   113,  807,    0]
bt18 ( 126, 113, 144) [   113,  920,    0]
bt17 ( 118, 108, 100) [   113,  807,  144]
bt17 ( 118, 108, 100) [   113,  915,  144]
bt17 ( 118, 108, 100) [     0, 1051,    0]
 bt9 (  77,  74,  67) [     0, 1051,  100]
 bt3 (  43,  41,  36) [     0, 1051,  167]
bt15 ( 116, 100, 129) [   118, 1033,    0]
bt12 (  95,  82,  92) [   118, 1033,  129]
bt14 ( 200, 178,  96) [     0, 1159,    0]
bt14 ( 200, 178,  96) [     0, 1159,   96]
 bt3 (  43,  41,  36) [     0, 1159,  192]


Cost = 286296490