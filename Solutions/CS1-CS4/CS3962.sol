%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instance: CS3962
% Mon Mar  1 18:39:53 2010
% Syntax:
% NumUsedContainers
% IdContainerType (length,width,height) NumBoxes
% IdBox (length,width,height) container [x, y, z]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

5

ct0 ( 242,1360, 245) 7
bt21 (  86, 123, 122) [     0,    0,    0]
bt24 (  96, 200, 178) [     0,  123,    0]
bt31 ( 142, 154, 179) [    96,    0,    0]
bt31 ( 142, 154, 179) [    96,  154,    0]
bt31 ( 154, 142, 179) [     0,  323,    0]
bt31 ( 142, 154, 179) [     0,  465,    0]
 bt4 (  42,  39,  38) [     0,  465,  179]

ct0 ( 242,1360, 245) 50
bt27 ( 116, 101, 197) [     0,    0,    0]
bt27 ( 116, 101, 197) [   116,    0,    0]
bt27 ( 116, 101, 197) [     0,  101,    0]
bt27 ( 116, 101, 197) [   116,  101,    0]
bt27 ( 116, 101, 197) [     0,  202,    0]
bt27 ( 116, 101, 197) [   116,  202,    0]
bt30 ( 137, 126, 161) [     0,  303,    0]
bt30 ( 137, 126, 161) [     0,  429,    0]
bt18 (  77,  79,  69) [     0,  429,  161]
bt22 (  90,  95, 136) [   137,  303,    0]
bt22 (  90,  95, 136) [   137,  398,    0]
bt18 (  77,  79,  69) [   137,  303,  136]
bt30 ( 137, 126, 161) [     0,  555,    0]
bt12 ( 124,  57,  75) [     0,  555,  161]
bt10 (  54,  61,  60) [   137,  493,    0]
 bt3 (  38,  48,  43) [   137,  493,   60]
 bt3 (  38,  48,  43) [   137,  493,  103]
 bt3 (  38,  48,  43) [   137,  493,  146]
bt31 ( 154, 142, 179) [     0,  681,    0]
 bt7 (  44,  49,  50) [     0,  681,  179]
bt18 (  77,  79,  69) [   154,  554,    0]
 bt4 (  39,  42,  38) [   154,  554,   69]
bt30 ( 161, 126, 137) [     0,  823,    0]
bt18 (  77,  79,  69) [     0,  823,  137]
bt18 (  77,  79,  69) [    77,  823,  137]
bt18 (  77,  79,  69) [   161,  633,    0]
bt18 (  77,  79,  69) [   161,  712,    0]
bt18 (  77,  79,  69) [   161,  791,    0]
bt18 (  77,  79,  69) [   161,  870,    0]
bt18 (  77,  79,  69) [   161,  633,   69]
bt18 (  77,  79,  69) [   161,  712,   69]
bt18 (  77,  79,  69) [   161,  791,   69]
bt18 (  77,  79,  69) [   161,  870,   69]
bt18 (  77,  79,  69) [   161,  633,  138]
bt18 (  77,  79,  69) [   161,  712,  138]
bt30 ( 161, 126, 137) [     0,  949,    0]
bt30 ( 161, 126, 137) [     0, 1075,    0]
bt18 (  79,  77,  69) [     0, 1075,  137]
bt18 (  79,  77,  69) [    79, 1075,  137]
bt18 (  79,  77,  69) [   161,  949,    0]
bt18 (  79,  77,  69) [   161, 1026,    0]
bt18 (  79,  77,  69) [   161, 1103,    0]
bt18 (  79,  77,  69) [   161,  949,   69]
bt18 (  79,  77,  69) [   161, 1026,   69]
bt18 (  79,  77,  69) [   161, 1103,   69]
bt18 (  79,  77,  69) [   161,  949,  138]
bt18 (  79,  77,  69) [   161, 1026,  138]
bt18 (  79,  77,  69) [   161, 1103,  138]
bt31 ( 142, 154, 179) [     0, 1201,    0]
 bt3 (  48,  38,  43) [     0, 1201,  179]

ct0 ( 242,1360, 245) 93
bt33 ( 168, 155, 195) [     0,    0,    0]
bt11 (  55,  58,  49) [     0,    0,  195]
bt11 (  55,  58,  49) [    55,    0,  195]
bt11 (  55,  58,  49) [   110,    0,  195]
bt11 (  55,  58,  49) [     0,   58,  195]
bt11 (  55,  58,  49) [    55,   58,  195]
bt15 (  74,  67,  77) [   168,    0,    0]
bt15 (  74,  67,  77) [   168,   67,    0]
bt15 (  67,  77,  74) [   168,    0,   77]
bt28 ( 113, 126, 144) [     0,  155,    0]
bt14 (  64,  68,  57) [     0,  155,  144]
bt26 ( 116, 100, 129) [   113,  155,    0]
bt15 (  67,  77,  74) [   113,  155,  129]
bt28 ( 126, 113, 144) [     0,  281,    0]
bt11 (  58,  55,  49) [     0,  281,  144]
bt11 (  58,  55,  49) [    58,  281,  144]
bt11 (  58,  55,  49) [     0,  336,  144]
bt11 (  58,  55,  49) [    58,  336,  144]
bt11 (  58,  55,  49) [     0,  281,  193]
bt11 (  58,  55,  49) [    58,  281,  193]
bt11 (  58,  55,  49) [     0,  336,  193]
bt11 (  58,  55,  49) [    58,  336,  193]
bt28 ( 126, 113, 144) [     0,  394,    0]
bt15 (  77,  67,  74) [     0,  394,  144]
bt28 ( 126, 113, 144) [     0,  507,    0]
bt15 (  67,  77,  74) [     0,  507,  144]
bt28 ( 126, 113, 144) [     0,  620,    0]
bt11 (  58,  55,  49) [     0,  620,  144]
bt11 (  58,  55,  49) [    58,  620,  144]
bt11 (  58,  55,  49) [     0,  675,  144]
 bt8 (  46,  45,  41) [     0,  620,  193]
 bt8 (  46,  45,  41) [     0,  665,  193]
bt28 ( 126, 113, 144) [     0,  733,    0]
bt15 (  77,  74,  67) [     0,  733,  144]
bt28 ( 126, 113, 144) [     0,  846,    0]
bt14 (  64,  68,  57) [     0,  846,  144]
 bt8 (  45,  46,  41) [     0,  846,  201]
bt26 ( 116, 100, 129) [   126,  255,    0]
bt26 ( 116, 100, 129) [   126,  355,    0]
bt26 ( 116, 100, 129) [   126,  455,    0]
bt26 ( 116, 100, 129) [   126,  555,    0]
bt26 ( 116, 100, 129) [   126,  655,    0]
bt26 ( 116, 100, 129) [   126,  755,    0]
bt26 ( 116, 100, 129) [   126,  855,    0]
bt17 (  85,  75,  86) [   126,  255,  129]
bt17 (  85,  75,  86) [   126,  330,  129]
bt17 (  85,  75,  86) [   126,  405,  129]
bt17 (  85,  75,  86) [   126,  480,  129]
bt17 (  85,  75,  86) [   126,  555,  129]
bt17 (  85,  75,  86) [   126,  630,  129]
bt17 (  85,  75,  86) [   126,  705,  129]
bt17 (  85,  75,  86) [   126,  780,  129]
bt17 (  85,  75,  86) [   126,  855,  129]
bt24 ( 200,  96, 178) [     0,  959,    0]
bt13 (  68,  69,  61) [     0,  959,  178]
bt13 (  68,  69,  61) [    68,  959,  178]
 bt5 (  42,  40,  38) [   200,  955,    0]
 bt5 (  42,  40,  38) [   200,  995,    0]
 bt5 (  40,  42,  38) [   200,  955,   38]
 bt1 (  38,  42,  33) [   200,  955,   76]
 bt0 (  32,  36,  31) [   200,  955,  109]
bt24 ( 200,  96, 178) [     0, 1055,    0]
bt16 (  69,  68,  61) [     0, 1055,  178]
bt16 (  69,  68,  61) [    69, 1055,  178]
 bt2 (  40,  37,  33) [   200, 1035,    0]
 bt2 (  40,  37,  33) [   200, 1072,    0]
 bt2 (  40,  37,  33) [   200, 1109,    0]
 bt2 (  40,  37,  33) [   200, 1035,   33]
 bt2 (  40,  37,  33) [   200, 1072,   33]
 bt2 (  40,  37,  33) [   200, 1109,   33]
 bt2 (  40,  37,  33) [   200, 1035,   66]
 bt2 (  40,  37,  33) [   200, 1072,   66]
 bt2 (  40,  37,  33) [   200, 1109,   66]
 bt2 (  40,  37,  33) [   200, 1035,   99]
 bt2 (  40,  37,  33) [   200, 1072,   99]
 bt2 (  40,  37,  33) [   200, 1109,   99]
 bt2 (  40,  37,  33) [   200, 1035,  132]
 bt2 (  40,  37,  33) [   200, 1072,  132]
 bt0 (  32,  36,  31) [   200, 1035,  165]
 bt0 (  32,  36,  31) [   200, 1071,  165]
 bt0 (  32,  36,  31) [   200, 1035,  196]
 bt0 (  32,  36,  31) [   200, 1071,  196]
bt24 ( 200,  96, 178) [     0, 1151,    0]
bt13 (  69,  68,  61) [     0, 1151,  178]
bt13 (  69,  68,  61) [    69, 1151,  178]
 bt6 (  42,  48,  43) [   200, 1146,    0]
 bt6 (  42,  48,  43) [   200, 1194,    0]
 bt6 (  42,  48,  43) [   200, 1146,   43]
 bt6 (  42,  48,  43) [   200, 1194,   43]
 bt2 (  37,  40,  33) [   200, 1146,   86]
 bt2 (  37,  40,  33) [   200, 1186,   86]
bt24 ( 200,  96, 178) [     0, 1247,    0]
 bt9 (  55,  49,  49) [     0, 1247,  178]

ct0 ( 242,1360, 245) 42
bt28 ( 126, 113, 144) [     0,    0,    0]
bt15 (  67,  74,  77) [     0,    0,  144]
bt26 ( 116, 100, 129) [   126,    0,    0]
bt15 (  74,  67,  77) [   126,    0,  129]
bt28 ( 126, 113, 144) [     0,  113,    0]
bt15 (  67,  77,  74) [     0,  113,  144]
bt26 ( 116, 100, 129) [   126,  100,    0]
bt15 (  67,  74,  77) [   126,  100,  129]
bt32 ( 149, 154, 148) [     0,  226,    0]
bt23 (  92, 106, 101) [   149,  200,    0]
bt17 (  85,  86,  75) [   149,  200,  101]
bt32 ( 154, 149, 148) [     0,  380,    0]
bt13 (  68,  61,  69) [     0,  380,  148]
bt13 (  68,  61,  69) [    68,  380,  148]
bt13 (  68,  69,  61) [   154,  306,    0]
bt13 (  68,  61,  69) [   154,  306,   61]
bt32 ( 154, 149, 148) [     0,  529,    0]
bt17 (  75,  86,  85) [     0,  529,  148]
bt17 (  75,  86,  85) [    75,  529,  148]
bt20 (  85,  75,  86) [   154,  375,    0]
bt20 (  85,  75,  86) [   154,  450,    0]
bt13 (  68,  69,  61) [   154,  375,   86]
bt32 ( 154, 149, 148) [     0,  678,    0]
bt19 (  82,  95,  92) [     0,  678,  148]
bt20 (  86,  85,  75) [   154,  525,    0]
bt20 (  75,  85,  86) [   154,  525,   75]
bt32 ( 154, 149, 148) [     0,  827,    0]
bt13 (  61,  68,  69) [     0,  827,  148]
bt13 (  61,  68,  69) [    61,  827,  148]
bt19 (  82,  95,  92) [   154,  610,    0]
bt19 (  82,  95,  92) [   154,  705,    0]
bt19 (  82,  95,  92) [   154,  800,    0]
bt13 (  68,  69,  61) [   154,  610,   92]
bt13 (  68,  69,  61) [   154,  679,   92]
bt13 (  61,  69,  68) [   154,  610,  153]
bt27 ( 116, 101, 197) [     0,  976,    0]
bt27 ( 116, 101, 197) [   116,  976,    0]
bt13 (  69,  61,  68) [     0, 1077,    0]
bt13 (  68,  61,  69) [     0, 1077,   68]
bt24 (  96, 200, 178) [     0, 1138,    0]
bt30 ( 137, 126, 161) [    96, 1077,    0]
bt30 ( 137, 126, 161) [    96, 1203,    0]

ct0 ( 242,1360, 245) 20
bt28 ( 113, 126, 144) [     0,    0,    0]
bt29 ( 126, 120, 176) [   113,    0,    0]
bt25 ( 107,  98, 147) [     0,  126,    0]
bt14 (  64,  68,  57) [     0,  126,  147]
bt28 ( 113, 126, 144) [     0,  224,    0]
bt28 ( 113, 126, 144) [   113,  120,    0]
bt28 ( 113, 126, 144) [     0,  350,    0]
bt28 ( 126, 113, 144) [   113,  246,    0]
bt28 ( 126, 113, 144) [   113,  359,    0]
bt14 (  68,  64,  57) [   113,  246,  144]
bt28 ( 113, 126, 144) [     0,  476,    0]
bt28 ( 113, 126, 144) [   113,  472,    0]
bt28 ( 113, 126, 144) [     0,  602,    0]
bt28 ( 113, 126, 144) [   113,  598,    0]
bt28 ( 113, 126, 144) [     0,  728,    0]
bt28 ( 113, 126, 144) [   113,  724,    0]
bt26 ( 116, 100, 129) [     0,  854,    0]
bt26 ( 116, 100, 129) [   116,  850,    0]
bt33 ( 155, 168, 195) [     0,  954,    0]
 bt8 (  46,  45,  41) [     0,  954,  195]


Cost = 794972318