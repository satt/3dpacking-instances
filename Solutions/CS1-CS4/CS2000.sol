%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instance: CSCase-0
% Mon Mar  1 18:39:45 2010
% Syntax:
% NumUsedContainers
% IdContainerType (length,width,height) NumBoxes
% IdBox (length,width,height) container [x, y, z]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

1

ct0 ( 233,1194, 238) 165
 bt3 (  99,  63, 106) [     0,    0,    0]
 bt3 (  99,  63, 106) [     0,    0,  106]
 bt3 (  99,  63, 106) [    99,    0,    0]
 bt3 (  99,  63, 106) [    99,    0,  106]
 bt3 (  99,  63, 106) [     0,   63,    0]
 bt3 (  99,  63, 106) [     0,   63,  106]
 bt3 (  99,  63, 106) [    99,   63,    0]
 bt3 (  99,  63, 106) [    99,   63,  106]
 bt5 (  25,  88, 139) [     0,  126,    0]
 bt5 (  25,  88, 139) [    25,  126,    0]
 bt5 (  25,  88, 139) [    50,  126,    0]
 bt5 (  88,  25, 139) [    75,  126,    0]
 bt5 (  88,  25, 139) [    75,  151,    0]
 bt5 (  88,  25, 139) [    75,  176,    0]
 bt3 (  99,  63, 106) [     0,  214,    0]
 bt3 (  99,  63, 106) [     0,  214,  106]
 bt5 (  88,  25, 139) [    99,  201,    0]
 bt5 (  88,  25, 139) [    99,  226,    0]
 bt3 (  99,  63, 106) [     0,  277,    0]
 bt3 (  99,  63, 106) [     0,  277,  106]
 bt5 (  25,  88, 139) [     0,  340,    0]
 bt5 (  25,  88, 139) [    25,  340,    0]
 bt5 (  25,  88, 139) [    50,  340,    0]
 bt5 (  25,  88, 139) [    75,  340,    0]
 bt5 (  25,  88, 139) [   100,  251,    0]
 bt5 (  25,  88, 139) [   100,  339,    0]
 bt5 (  25, 139,  88) [   100,  251,  139]
 bt5 (  25,  88, 139) [   125,  251,    0]
 bt5 (  25,  88, 139) [   125,  339,    0]
 bt5 (  25,  88, 139) [   150,  251,    0]
 bt5 (  25,  88, 139) [   150,  339,    0]
 bt5 (  25,  88, 139) [   175,  251,    0]
 bt5 (  25,  88, 139) [   175,  339,    0]
 bt5 (  25, 139,  88) [   175,  251,  139]
 bt3 (  99,  63, 106) [     0,  428,    0]
 bt3 (  99,  63, 106) [     0,  428,  106]
 bt3 (  99,  63, 106) [    99,  428,    0]
 bt3 (  99,  63, 106) [    99,  428,  106]
 bt3 (  99,  63, 106) [     0,  491,    0]
 bt3 (  99,  63, 106) [     0,  491,  106]
 bt3 (  99,  63, 106) [    99,  491,    0]
 bt3 (  99,  63, 106) [    99,  491,  106]
 bt3 (  99,  63, 106) [     0,  554,    0]
 bt3 (  99,  63, 106) [     0,  554,  106]
 bt3 (  99,  63, 106) [    99,  554,    0]
 bt3 (  99,  63, 106) [    99,  554,  106]
 bt5 (  25,  88, 139) [     0,  617,    0]
 bt5 (  25,  88, 139) [    25,  617,    0]
 bt5 (  25,  88, 139) [    50,  617,    0]
 bt5 (  25,  88, 139) [    75,  617,    0]
 bt5 (  25,  88, 139) [   100,  617,    0]
 bt5 (  25,  88, 139) [   125,  617,    0]
 bt5 (  25,  88, 139) [   150,  617,    0]
 bt5 (  25,  88, 139) [   175,  617,    0]
 bt5 (  25,  88, 139) [   200,    0,    0]
 bt5 (  25,  88, 139) [   200,   88,    0]
 bt5 (  25,  88, 139) [   200,  176,    0]
 bt5 (  25,  88, 139) [   200,  264,    0]
 bt5 (  25,  88, 139) [   200,  352,    0]
 bt5 (  25,  88, 139) [   200,  440,    0]
 bt5 (  25,  88, 139) [   200,  528,    0]
 bt5 (  25,  88, 139) [   200,  616,    0]
 bt5 (  25, 139,  88) [   200,    0,  139]
 bt5 (  25, 139,  88) [   200,  139,  139]
 bt5 (  25, 139,  88) [   200,  278,  139]
 bt5 (  25, 139,  88) [   200,  417,  139]
 bt5 (  25, 139,  88) [   200,  556,  139]
 bt2 (  40,  64, 228) [     0,  705,    0]
 bt2 (  40,  64, 228) [    40,  705,    0]
 bt2 (  40,  64, 228) [    80,  705,    0]
 bt2 (  40,  64, 228) [   120,  705,    0]
 bt2 (  40,  64, 228) [   160,  705,    0]
 bt2 (  40,  64, 228) [     0,  769,    0]
 bt2 (  40,  64, 228) [    40,  769,    0]
 bt2 (  40,  64, 228) [    80,  769,    0]
 bt2 (  40,  64, 228) [   120,  769,    0]
 bt2 (  40,  64, 228) [   160,  769,    0]
 bt2 (  40,  64, 228) [     0,  833,    0]
 bt2 (  40,  64, 228) [    40,  833,    0]
 bt2 (  40,  64, 228) [    80,  833,    0]
 bt2 (  40,  64, 228) [   120,  833,    0]
 bt2 (  40,  64, 228) [   160,  833,    0]
 bt0 (  25,  25,  38) [   200,  704,    0]
 bt0 (  25,  25,  38) [   200,  729,    0]
 bt0 (  25,  25,  38) [   200,  754,    0]
 bt0 (  25,  25,  38) [   200,  779,    0]
 bt0 (  25,  25,  38) [   200,  804,    0]
 bt0 (  25,  25,  38) [   200,  829,    0]
 bt0 (  25,  25,  38) [   200,  854,    0]
 bt1 (  38,  50,  63) [     0,  897,    0]
 bt1 (  38,  50,  63) [     0,  897,   63]
 bt0 (  38,  25,  25) [     0,  897,  126]
 bt0 (  38,  25,  25) [     0,  922,  126]
 bt1 (  38,  50,  63) [     0,  897,  151]
 bt1 (  38,  50,  63) [    38,  897,    0]
 bt1 (  38,  50,  63) [    38,  897,   63]
 bt0 (  38,  25,  25) [    38,  897,  126]
 bt0 (  38,  25,  25) [    38,  922,  126]
 bt1 (  38,  50,  63) [    38,  897,  151]
 bt1 (  38,  50,  63) [    76,  897,    0]
 bt1 (  38,  50,  63) [    76,  897,   63]
 bt1 (  38,  50,  63) [    76,  897,  126]
 bt0 (  25,  25,  38) [    76,  897,  189]
 bt0 (  25,  25,  38) [    76,  922,  189]
 bt1 (  38,  50,  63) [   114,  897,    0]
 bt1 (  38,  50,  63) [   114,  897,   63]
 bt1 (  38,  50,  63) [   114,  897,  126]
 bt0 (  38,  25,  25) [   114,  897,  189]
 bt0 (  38,  25,  25) [   114,  922,  189]
 bt1 (  38,  50,  63) [   152,  897,    0]
 bt1 (  38,  50,  63) [   152,  897,   63]
 bt1 (  38,  50,  63) [   152,  897,  126]
 bt1 (  38,  50,  63) [   190,  897,    0]
 bt1 (  38,  50,  63) [   190,  897,   63]
 bt1 (  38,  50,  63) [   190,  897,  126]
 bt0 (  38,  25,  25) [   190,  897,  189]
 bt0 (  38,  25,  25) [   190,  922,  189]
 bt4 (  81,  66, 114) [     0,  947,    0]
 bt4 (  81,  66, 114) [     0,  947,  114]
 bt0 (  25,  25,  38) [    81,  947,    0]
 bt0 (  25,  25,  38) [    81,  972,    0]
 bt4 (  81,  66, 114) [   106,  947,    0]
 bt4 (  81,  66, 114) [   106,  947,  114]
 bt2 (  40,  64, 228) [   187,  947,    0]
 bt2 (  40,  64, 228) [     0, 1013,    0]
 bt2 (  40,  64, 228) [    40, 1013,    0]
 bt2 (  40,  64, 228) [    80, 1013,    0]
 bt2 (  40,  64, 228) [   120, 1013,    0]
 bt2 (  64,  40, 228) [   160, 1013,    0]
 bt4 (  81,  66, 114) [     0, 1077,    0]
 bt4 (  81,  66, 114) [     0, 1077,  114]
 bt4 (  81,  66, 114) [    81, 1077,    0]
 bt4 (  81,  66, 114) [    81, 1077,  114]
 bt2 (  64,  40, 228) [   162, 1053,    0]
 bt2 (  64,  40, 228) [   162, 1093,    0]
 bt1 (  38,  50,  63) [     0, 1143,    0]
 bt1 (  38,  50,  63) [     0, 1143,   63]
 bt1 (  38,  50,  63) [     0, 1143,  126]
 bt0 (  25,  25,  38) [     0, 1143,  189]
 bt0 (  25,  25,  38) [     0, 1168,  189]
 bt1 (  38,  50,  63) [    38, 1143,    0]
 bt1 (  38,  50,  63) [    38, 1143,   63]
 bt1 (  38,  50,  63) [    38, 1143,  126]
 bt0 (  25,  25,  38) [    38, 1143,  189]
 bt0 (  25,  25,  38) [    38, 1168,  189]
 bt1 (  38,  50,  63) [    76, 1143,    0]
 bt1 (  38,  50,  63) [    76, 1143,   63]
 bt0 (  38,  25,  25) [    76, 1143,  126]
 bt0 (  38,  25,  25) [    76, 1168,  126]
 bt1 (  38,  50,  63) [    76, 1143,  151]
 bt1 (  38,  50,  63) [   114, 1143,    0]
 bt1 (  38,  50,  63) [   114, 1143,   63]
 bt1 (  38,  50,  63) [   114, 1143,  126]
 bt0 (  25,  25,  38) [   114, 1143,  189]
 bt0 (  25,  25,  38) [   114, 1168,  189]
 bt1 (  38,  50,  63) [   152, 1143,    0]
 bt1 (  38,  50,  63) [   152, 1143,   63]
 bt1 (  38,  50,  63) [   152, 1143,  126]
 bt0 (  38,  25,  25) [   152, 1143,  189]
 bt0 (  38,  25,  25) [   152, 1168,  189]
 bt1 (  38,  50,  63) [   190, 1133,    0]
 bt1 (  38,  50,  63) [   190, 1133,   63]
 bt1 (  38,  50,  63) [   190, 1133,  126]
 bt0 (  25,  25,  38) [   190, 1133,  189]
 bt0 (  25,  25,  38) [   190, 1158,  189]

 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt1 (  38,  50,  63)  [   --,  --,  --]
 bt1 (  38,  50,  63)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt2 (  40, 228,  64)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt1 (  38,  50,  63)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt2 (  40, 228,  64)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt1 (  38,  50,  63)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt1 (  38,  50,  63)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt1 (  38,  50,  63)  [   --,  --,  --]
 bt1 (  38,  50,  63)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt1 (  38,  50,  63)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt1 (  38,  50,  63)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt0 (  25,  38,  25)  [   --,  --,  --]
 bt4 (  66,  81, 114)  [   --,  --,  --]

Cost = 370067700