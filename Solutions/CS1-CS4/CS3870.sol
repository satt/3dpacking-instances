%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instance: CS3870
% Mon Mar  1 18:40:11 2010
% Syntax:
% NumUsedContainers
% IdContainerType (length,width,height) NumBoxes
% IdBox (length,width,height) container [x, y, z]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

2

ct0 ( 242,1360, 245) 68
bt19 ( 116, 129, 100) [     0,    0,    0]
bt19 ( 116, 129, 100) [     0,    0,  100]
 bt0 (  34,  30,  37) [     0,    0,  200]
 bt0 (  34,  30,  37) [    34,    0,  200]
 bt0 (  34,  30,  37) [    68,    0,  200]
 bt0 (  34,  30,  37) [     0,   30,  200]
bt19 ( 116, 129, 100) [   116,    0,    0]
bt19 ( 116, 129, 100) [   116,    0,  100]
 bt2 (  45,  46,  41) [   116,    0,  200]
 bt2 (  45,  46,  41) [   161,    0,  200]
 bt2 (  45,  46,  41) [   116,   46,  200]
bt23 ( 137, 126, 161) [     0,  129,    0]
bt13 (  80, 120, 103) [   137,  129,    0]
bt13 (  80, 120, 103) [   137,  129,  103]
 bt6 (  55,  49,  55) [     0,  255,    0]
 bt3 (  50,  44,  49) [     0,  255,   55]
 bt6 (  55,  49,  55) [    55,  255,    0]
 bt6 (  55,  49,  55) [    55,  255,   55]
 bt6 (  55,  49,  55) [    55,  255,  110]
 bt6 (  55,  49,  55) [    55,  255,  165]
bt12 (  86,  75,  85) [     0,  304,    0]
bt12 (  86,  75,  85) [     0,  304,   85]
 bt9 (  77,  67,  74) [     0,  304,  170]
bt22 ( 113, 126, 144) [     0,  379,    0]
bt16 ( 106, 101,  92) [     0,  379,  144]
bt22 ( 126, 113, 144) [   113,  255,    0]
bt22 ( 126, 113, 144) [   113,  368,    0]
bt12 (  86,  75,  85) [   113,  255,  144]
bt12 (  86,  75,  85) [   113,  330,  144]
bt12 (  86,  75,  85) [   113,  405,  144]
bt12 (  86,  75,  85) [     0,  505,    0]
bt12 (  86,  75,  85) [     0,  505,   85]
 bt9 (  77,  67,  74) [     0,  505,  170]
bt22 ( 126, 113, 144) [     0,  580,    0]
bt21 ( 118, 108, 100) [     0,  580,  144]
bt22 ( 126, 113, 144) [     0,  693,    0]
bt12 (  75,  86,  85) [     0,  693,  144]
bt20 ( 116, 101, 197) [   126,  481,    0]
bt20 ( 116, 101, 197) [   126,  582,    0]
bt20 ( 116, 101, 197) [   126,  683,    0]
 bt3 (  50,  49,  44) [   126,  481,  197]
 bt3 (  50,  49,  44) [   176,  481,  197]
bt12 (  86,  75,  85) [     0,  806,    0]
bt12 (  86,  75,  85) [     0,  806,   85]
 bt9 (  77,  67,  74) [     0,  806,  170]
bt10 (  69,  68,  61) [    86,  806,    0]
bt10 (  69,  68,  61) [    86,  806,   61]
bt10 (  69,  68,  61) [    86,  806,  122]
bt12 (  86,  75,  85) [   155,  784,    0]
bt12 (  86,  75,  85) [   155,  784,   85]
 bt9 (  77,  67,  74) [   155,  784,  170]
bt22 ( 113, 126, 144) [     0,  881,    0]
bt16 ( 106,  92, 101) [     0,  881,  144]
bt22 ( 126, 113, 144) [   113,  874,    0]
bt21 ( 118, 108, 100) [   113,  874,  144]
bt22 ( 113, 126, 144) [     0, 1007,    0]
bt12 (  86,  75,  85) [     0, 1007,  144]
bt22 ( 126, 113, 144) [   113,  987,    0]
bt12 (  75,  86,  85) [   113,  987,  144]
 bt9 (  77,  67,  74) [     0, 1133,    0]
 bt9 (  77,  67,  74) [     0, 1133,   74]
 bt9 (  77,  67,  74) [     0, 1133,  148]
bt22 ( 113, 126, 144) [     0, 1200,    0]
bt12 (  75,  86,  85) [     0, 1200,  144]
bt22 ( 126, 113, 144) [   113, 1100,    0]
bt22 ( 126, 113, 144) [   113, 1213,    0]
bt12 (  86,  75,  85) [   113, 1100,  144]
bt12 (  86,  75,  85) [   113, 1175,  144]

ct0 ( 242,1360, 245) 49
bt19 ( 100, 129, 116) [     0,    0,    0]
bt19 ( 100, 116, 129) [     0,    0,  116]
 bt8 (  61,  69,  68) [   100,    0,    0]
 bt8 (  61,  69,  68) [   100,    0,   68]
 bt7 (  60,  61,  54) [   100,    0,  136]
 bt7 (  60,  61,  54) [   100,    0,  190]
bt11 (  67,  74,  77) [   161,    0,    0]
 bt4 (  55,  49,  55) [   161,    0,   77]
 bt4 (  55,  49,  55) [   161,    0,  132]
 bt4 (  55,  49,  55) [   161,    0,  187]
bt19 ( 100, 129, 116) [     0,  129,    0]
bt19 ( 100, 129, 116) [     0,  129,  116]
bt19 ( 129, 100, 116) [   100,   74,    0]
bt19 ( 116, 100, 129) [   100,   74,  116]
bt19 ( 100, 129, 116) [     0,  258,    0]
bt19 ( 100, 129, 116) [     0,  258,  116]
bt19 ( 116, 100, 129) [   100,  174,    0]
bt19 ( 116, 100, 129) [   100,  274,    0]
bt17 (  95,  92,  82) [   100,  174,  129]
bt17 (  95,  92,  82) [   100,  266,  129]
bt11 (  77,  67,  74) [     0,  387,    0]
bt11 (  77,  67,  74) [     0,  387,   74]
bt11 (  77,  67,  74) [     0,  387,  148]
 bt8 (  69,  61,  68) [    77,  387,    0]
 bt7 (  60,  61,  54) [    77,  387,   68]
 bt7 (  60,  61,  54) [    77,  387,  122]
bt11 (  77,  67,  74) [   146,  374,    0]
bt11 (  77,  67,  74) [   146,  374,   74]
 bt4 (  49,  55,  55) [   146,  374,  148]
bt14 (  95,  82,  92) [     0,  454,    0]
bt15 (  85,  75,  86) [     0,  454,   92]
 bt5 (  54,  61,  60) [     0,  454,  178]
bt14 (  82,  95,  92) [     0,  536,    0]
bt14 (  82,  95,  92) [     0,  536,   92]
bt18 (  96, 200, 178) [     0,  631,    0]
 bt5 (  54,  61,  60) [     0,  631,  178]
bt23 ( 137, 126, 161) [    96,  448,    0]
bt23 ( 137, 126, 161) [    96,  574,    0]
bt23 ( 137, 126, 161) [    96,  700,    0]
bt23 ( 137, 126, 161) [     0,  831,    0]
bt23 ( 137, 126, 161) [     0,  957,    0]
bt14 (  95,  82,  92) [   137,  826,    0]
bt14 (  95,  82,  92) [   137,  908,    0]
bt14 (  95,  82,  92) [   137,  990,    0]
bt15 (  85,  75,  86) [   137,  826,   92]
bt15 (  85,  75,  86) [   137,  901,   92]
bt15 (  85,  75,  86) [   137,  976,   92]
bt24 ( 149, 154, 148) [     0, 1083,    0]
 bt1 (  40,  33,  37) [     0, 1083,  148]


Cost = 348125060