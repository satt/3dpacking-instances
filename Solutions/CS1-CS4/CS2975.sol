%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instance: CS2975
% Mon Mar  1 18:39:47 2010
% Syntax:
% NumUsedContainers
% IdContainerType (length,width,height) NumBoxes
% IdBox (length,width,height) container [x, y, z]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

2

ct0 ( 235,1180, 235) 38
bt21 ( 168, 155, 195) [     0,    0,    0]
bt21 ( 168, 155, 195) [     0,  155,    0]
bt21 ( 168, 155, 195) [     0,  310,    0]
bt11 (  67,  77,  74) [   168,    0,    0]
bt11 (  67,  77,  74) [   168,   77,    0]
bt11 (  67,  77,  74) [   168,  154,    0]
bt11 (  67,  77,  74) [   168,  231,    0]
bt11 (  67,  77,  74) [   168,  308,    0]
bt11 (  67,  77,  74) [   168,  385,    0]
bt18 ( 116, 101, 197) [     0,  465,    0]
 bt9 (  60,  61,  54) [   116,  465,    0]
 bt9 (  60,  61,  54) [   116,  465,   54]
 bt7 (  54,  61,  60) [   116,  465,  108]
 bt7 (  54,  61,  60) [   116,  465,  168]
 bt7 (  54,  61,  60) [   176,  462,    0]
 bt7 (  54,  61,  60) [   176,  462,   60]
 bt7 (  54,  61,  60) [   176,  462,  120]
bt20 ( 137, 126, 161) [     0,  566,    0]
bt20 ( 137, 126, 161) [     0,  692,    0]
bt11 (  77,  67,  74) [     0,  692,  161]
bt15 (  95,  82,  92) [   137,  526,    0]
bt15 (  95,  82,  92) [   137,  608,    0]
bt20 ( 137, 126, 161) [     0,  818,    0]
bt20 ( 137, 126, 161) [     0,  944,    0]
bt13 (  74,  77,  67) [     0,  944,  161]
bt14 (  86,  75,  85) [   137,  690,    0]
bt14 (  86,  75,  85) [   137,  765,    0]
bt14 (  86,  75,  85) [   137,  840,    0]
bt14 (  86,  75,  85) [   137,  915,    0]
bt14 (  86,  75,  85) [   137,  990,    0]
bt10 (  61,  69,  68) [   137,  690,   85]
bt10 (  61,  69,  68) [   137,  759,   85]
bt10 (  61,  69,  68) [   137,  828,   85]
bt10 (  61,  69,  68) [   137,  897,   85]
 bt6 (  49,  55,  55) [   137,  690,  153]
bt16 (  86,  85,  75) [     0, 1070,    0]
bt16 (  86,  85,  75) [     0, 1070,   75]
 bt0 (  37,  34,  30) [     0, 1070,  150]

ct2 ( 240,1340, 240) 51
bt19 ( 113, 126, 144) [     0,    0,    0]
 bt1 (  37,  40,  33) [     0,    0,  144]
bt19 ( 126, 113, 144) [   113,    0,    0]
bt21 ( 195, 155, 168) [     0,  126,    0]
bt17 ( 116, 100, 129) [     0,  281,    0]
bt17 ( 116, 100, 129) [   116,  281,    0]
bt17 ( 116, 100, 129) [     0,  381,    0]
bt17 ( 116, 100, 129) [   116,  381,    0]
bt17 ( 116, 100, 129) [     0,  481,    0]
bt17 ( 116, 100, 129) [   116,  481,    0]
bt17 ( 116, 100, 129) [     0,  581,    0]
bt17 ( 116, 100, 129) [   116,  581,    0]
bt17 ( 100, 116, 129) [     0,  681,    0]
bt19 ( 113, 126, 144) [     0,  797,    0]
bt12 (  69,  68,  61) [     0,  797,  144]
 bt4 (  46,  45,  41) [   113,  681,    0]
 bt3 (  49,  44,  50) [   159,  681,    0]
bt19 ( 113, 126, 144) [     0,  923,    0]
 bt3 (  49,  44,  50) [     0,  923,  144]
bt19 ( 126, 113, 144) [   113,  726,    0]
 bt4 (  46,  45,  41) [   113,  726,  144]
 bt1 (  37,  40,  33) [   113,  726,  185]
bt17 ( 100, 116, 129) [     0, 1049,    0]
bt12 (  68,  69,  61) [     0, 1049,  129]
bt19 ( 113, 126, 144) [     0, 1165,    0]
 bt3 (  44,  49,  50) [     0, 1165,  144]
 bt3 (  44,  49,  50) [    44, 1165,  144]
 bt3 (  44,  49,  50) [     0, 1214,  144]
bt19 ( 126, 113, 144) [   113,  839,    0]
bt19 ( 126, 113, 144) [   113,  952,    0]
bt19 ( 126, 113, 144) [   113, 1065,    0]
bt19 ( 126, 113, 144) [   113, 1178,    0]
bt15 (  95,  82,  92) [   113,  839,  144]
bt15 (  95,  82,  92) [   113,  921,  144]
bt15 (  95,  82,  92) [   113, 1003,  144]
bt15 (  95,  82,  92) [   113, 1085,  144]
bt15 (  95,  82,  92) [   113, 1167,  144]
 bt5 (  50,  49,  44) [     0, 1291,    0]
 bt5 (  50,  49,  44) [     0, 1291,   44]
 bt2 (  41,  43,  36) [     0, 1291,   88]
 bt2 (  36,  43,  41) [     0, 1291,  124]
 bt2 (  36,  41,  43) [     0, 1291,  165]
 bt8 (  55,  49,  55) [    50, 1291,    0]
 bt6 (  55,  49,  55) [    50, 1291,   55]
 bt6 (  55,  49,  55) [    50, 1291,  110]
 bt6 (  55,  49,  55) [    50, 1291,  165]
 bt6 (  55,  49,  55) [   105, 1291,    0]
 bt6 (  55,  49,  55) [   105, 1291,   55]
 bt6 (  55,  49,  55) [   105, 1291,  110]
 bt6 (  55,  49,  55) [   160, 1291,    0]
 bt6 (  55,  49,  55) [   160, 1291,   55]


Cost = 2171422589