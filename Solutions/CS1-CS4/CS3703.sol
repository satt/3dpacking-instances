%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instance: CS3703
% Mon Mar  1 18:39:34 2010
% Syntax:
% NumUsedContainers
% IdContainerType (length,width,height) NumBoxes
% IdBox (length,width,height) container [x, y, z]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

2

ct0 ( 235,1180, 235) 20
bt20 ( 118, 108, 100) [     0,    0,    0]
bt18 ( 116, 100, 129) [     0,    0,  100]
bt18 ( 116, 100, 129) [   118,    0,    0]
 bt7 (  77,  67,  74) [   118,    0,  129]
bt16 ( 107,  98, 147) [     0,  108,    0]
 bt3 (  54,  60,  61) [     0,  108,  147]
bt17 ( 118, 100, 108) [     0,  206,    0]
bt17 ( 118, 100, 108) [     0,  206,  108]
bt17 ( 118, 100, 108) [     0,  306,    0]
bt17 ( 118, 100, 108) [     0,  306,  108]
bt18 ( 116, 100, 129) [   118,  100,    0]
bt18 ( 116, 100, 129) [   118,  200,    0]
bt18 ( 116, 100, 129) [   118,  300,    0]
 bt7 (  77,  67,  74) [   118,  100,  129]
 bt7 (  77,  67,  74) [   118,  167,  129]
 bt7 (  77,  67,  74) [   118,  234,  129]
 bt7 (  77,  67,  74) [   118,  301,  129]
 bt7 (  77,  67,  74) [     0,  406,    0]
 bt7 (  77,  67,  74) [     0,  406,   74]
 bt3 (  61,  54,  60) [     0,  406,  148]

ct0 ( 235,1180, 235) 71
bt22 ( 113, 126, 144) [     0,    0,    0]
bt12 (  85,  86,  75) [     0,    0,  144]
bt22 ( 113, 126, 144) [   113,    0,    0]
bt12 (  85,  86,  75) [   113,    0,  144]
 bt6 (  69,  61,  68) [     0,  126,    0]
 bt6 (  69,  61,  68) [     0,  126,   68]
 bt6 (  69,  61,  68) [     0,  126,  136]
 bt6 (  69,  61,  68) [    69,  126,    0]
 bt6 (  69,  61,  68) [    69,  126,   68]
 bt6 (  69,  61,  68) [    69,  126,  136]
bt23 ( 142, 154, 179) [     0,  187,    0]
 bt1 (  43,  41,  36) [     0,  187,  179]
bt11 (  92,  95,  82) [   142,  126,    0]
bt11 (  92,  95,  82) [   142,  221,    0]
bt11 (  92,  95,  82) [   142,  126,   82]
bt11 (  92,  95,  82) [   142,  221,   82]
bt10 (  84,  80,  70) [   142,  126,  164]
bt10 (  84,  80,  70) [   142,  206,  164]
bt22 ( 113, 126, 144) [     0,  341,    0]
bt12 (  86,  85,  75) [     0,  341,  144]
 bt6 (  61,  69,  68) [   113,  341,    0]
 bt6 (  61,  69,  68) [   113,  341,   68]
 bt6 (  61,  69,  68) [   113,  341,  136]
 bt6 (  61,  69,  68) [   174,  316,    0]
 bt6 (  61,  69,  68) [   174,  385,    0]
 bt6 (  61,  69,  68) [   174,  316,   68]
 bt6 (  61,  69,  68) [   174,  385,   68]
 bt6 (  61,  69,  68) [   174,  316,  136]
 bt6 (  61,  69,  68) [   174,  385,  136]
bt22 ( 126, 113, 144) [     0,  467,    0]
bt12 (  86,  85,  75) [     0,  467,  144]
bt13 ( 107,  89, 103) [   126,  454,    0]
bt14 (  92,  82,  95) [   126,  454,  103]
 bt2 (  55,  49,  54) [     0,  580,    0]
 bt2 (  55,  49,  54) [     0,  580,   54]
 bt9 (  75,  86,  85) [     0,  629,    0]
 bt9 (  75,  86,  85) [     0,  629,   85]
 bt4 (  58,  64,  50) [     0,  629,  170]
 bt9 (  75,  85,  86) [    75,  580,    0]
 bt9 (  75,  85,  86) [    75,  580,   86]
 bt5 (  61,  60,  54) [    75,  580,  172]
 bt9 (  85,  86,  75) [   150,  543,    0]
 bt9 (  85,  86,  75) [   150,  629,    0]
 bt9 (  75,  85,  86) [   150,  543,   75]
 bt9 (  75,  85,  86) [   150,  628,   75]
 bt8 (  74,  77,  67) [   150,  543,  161]
 bt8 (  74,  77,  67) [   150,  620,  161]
bt21 ( 113, 140, 143) [     0,  715,    0]
 bt9 (  75,  86,  85) [     0,  715,  143]
bt19 ( 116, 101, 197) [   113,  715,    0]
 bt9 (  86,  85,  75) [     0,  855,    0]
 bt9 (  86,  85,  75) [     0,  855,   75]
 bt9 (  86,  85,  75) [     0,  855,  150]
 bt9 (  86,  85,  75) [    86,  855,    0]
 bt9 (  86,  85,  75) [    86,  855,   75]
 bt9 (  86,  85,  75) [    86,  855,  150]
 bt5 (  61,  60,  54) [   172,  816,    0]
 bt5 (  61,  60,  54) [   172,  876,    0]
 bt5 (  61,  60,  54) [   172,  816,   54]
 bt5 (  61,  60,  54) [   172,  876,   54]
 bt4 (  50,  64,  58) [   172,  816,  108]
bt15 ( 106,  92, 101) [     0,  940,    0]
bt15 ( 106,  92, 101) [     0,  940,  101]
bt15 ( 106,  92, 101) [   106,  940,    0]
bt15 ( 106,  92, 101) [   106,  940,  101]
bt15 (  92, 106, 101) [     0, 1032,    0]
bt15 (  92, 106, 101) [     0, 1032,  101]
 bt0 (  45,  36,  32) [     0, 1032,  202]
bt18 ( 116, 100, 129) [    92, 1032,    0]
 bt3 (  54,  60,  61) [    92, 1032,  129]
 bt3 (  54,  60,  61) [   146, 1032,  129]


Cost = 2061438079