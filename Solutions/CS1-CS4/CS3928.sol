%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instance: CS3928
% Mon Mar  1 18:39:53 2010
% Syntax:
% NumUsedContainers
% IdContainerType (length,width,height) NumBoxes
% IdBox (length,width,height) container [x, y, z]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

10

ct0 ( 242,1360, 245) 10
bt17 ( 154, 142, 179) [     0,    0,    0]
bt17 ( 154, 142, 179) [     0,  142,    0]
bt17 ( 154, 142, 179) [     0,  284,    0]
bt17 ( 154, 142, 179) [     0,  426,    0]
bt17 ( 154, 142, 179) [     0,  568,    0]
bt17 ( 154, 142, 179) [     0,  710,    0]
bt17 ( 154, 142, 179) [     0,  852,    0]
bt17 ( 154, 142, 179) [     0,  994,    0]
bt17 ( 154, 142, 179) [     0, 1136,    0]
 bt2 (  46,  45,  41) [     0, 1136,  179]

ct0 ( 242,1360, 245) 15
bt16 ( 138, 136, 197) [     0,    0,    0]
bt16 ( 197, 136, 138) [     0,  136,    0]
bt10 (  90, 136,  95) [     0,  136,  138]
bt16 ( 138, 136, 197) [     0,  272,    0]
bt16 ( 138, 136, 197) [     0,  408,    0]
bt10 (  90, 136,  95) [   138,  272,    0]
bt10 (  90, 136,  95) [   138,  408,    0]
bt16 ( 138, 136, 197) [     0,  544,    0]
bt16 ( 138, 136, 197) [     0,  680,    0]
bt16 ( 138, 136, 197) [     0,  816,    0]
bt16 ( 197, 136, 138) [     0,  952,    0]
bt10 (  95, 136,  90) [     0,  952,  138]
bt16 ( 197, 136, 138) [     0, 1088,    0]
bt16 ( 197, 136, 138) [     0, 1224,    0]
 bt5 (  69,  61,  68) [     0, 1224,  138]

ct0 ( 242,1360, 245) 11
bt18 ( 154, 149, 148) [     0,    0,    0]
bt18 ( 154, 149, 148) [     0,  149,    0]
bt18 ( 154, 149, 148) [     0,  298,    0]
bt18 ( 154, 149, 148) [     0,  447,    0]
bt18 ( 154, 149, 148) [     0,  596,    0]
bt18 ( 154, 149, 148) [     0,  745,    0]
bt18 ( 154, 149, 148) [     0,  894,    0]
bt12 ( 113, 144, 126) [     0, 1043,    0]
bt15 ( 126, 137, 161) [   113, 1043,    0]
bt15 ( 137, 161, 126) [     0, 1187,    0]
 bt2 (  46,  45,  41) [     0, 1187,  126]

ct0 ( 242,1360, 245) 9
bt19 ( 171, 163, 180) [     0,    0,    0]
bt19 ( 171, 163, 180) [     0,  163,    0]
bt19 ( 171, 163, 180) [     0,  326,    0]
bt19 ( 171, 163, 180) [     0,  489,    0]
bt19 ( 171, 163, 180) [     0,  652,    0]
bt19 ( 171, 163, 180) [     0,  815,    0]
bt19 ( 171, 163, 180) [     0,  978,    0]
bt19 ( 171, 163, 180) [     0, 1141,    0]
 bt1 (  49,  44,  50) [     0, 1141,  180]

ct0 ( 242,1360, 245) 8
bt18 ( 154, 149, 148) [     0,    0,    0]
bt18 ( 154, 149, 148) [     0,  149,    0]
bt18 ( 154, 149, 148) [     0,  298,    0]
bt18 ( 154, 149, 148) [     0,  447,    0]
bt18 ( 154, 149, 148) [     0,  596,    0]
bt18 ( 154, 149, 148) [     0,  745,    0]
bt18 ( 154, 149, 148) [     0,  894,    0]
 bt6 (  77,  67,  74) [     0,  894,  148]

ct0 ( 242,1360, 245) 21
bt15 ( 137, 126, 161) [     0,    0,    0]
bt15 ( 137, 126, 161) [     0,  126,    0]
bt15 ( 137, 126, 161) [     0,  252,    0]
bt15 ( 137, 126, 161) [     0,  378,    0]
bt15 ( 137, 126, 161) [     0,  504,    0]
bt15 ( 137, 126, 161) [     0,  630,    0]
bt15 ( 137, 126, 161) [     0,  756,    0]
bt15 ( 137, 126, 161) [     0,  882,    0]
bt15 ( 137, 126, 161) [     0, 1008,    0]
bt15 ( 137, 126, 161) [     0, 1134,    0]
bt11 (  98, 107, 147) [   137,    0,    0]
bt11 (  98, 107, 147) [   137,  107,    0]
bt11 (  98, 107, 147) [   137,  214,    0]
bt11 (  98, 107, 147) [   137,  321,    0]
bt11 (  98, 107, 147) [   137,  428,    0]
bt11 (  98, 107, 147) [   137,  535,    0]
bt11 (  98, 107, 147) [   137,  642,    0]
bt11 (  98, 107, 147) [   137,  749,    0]
bt11 (  98, 107, 147) [   137,  856,    0]
bt11 (  98, 107, 147) [   137,  963,    0]
bt11 (  98, 107, 147) [   137, 1070,    0]

ct0 ( 242,1360, 245) 10
bt16 ( 138, 136, 197) [     0,    0,    0]
bt16 ( 138, 136, 197) [     0,  136,    0]
bt17 ( 154, 142, 179) [     0,  272,    0]
bt17 ( 154, 142, 179) [     0,  414,    0]
bt17 ( 154, 142, 179) [     0,  556,    0]
bt17 ( 154, 142, 179) [     0,  698,    0]
bt16 ( 138, 136, 197) [     0,  840,    0]
bt16 ( 197, 136, 138) [     0,  976,    0]
bt17 ( 142, 154, 179) [     0, 1112,    0]
 bt2 (  46,  45,  41) [     0, 1112,  179]

ct0 ( 242,1360, 245) 11
bt18 ( 154, 149, 148) [     0,    0,    0]
bt15 ( 137, 126, 161) [     0,  149,    0]
bt17 ( 154, 142, 179) [     0,  275,    0]
bt15 ( 161, 126, 137) [     0,  417,    0]
bt15 ( 161, 126, 137) [     0,  543,    0]
bt15 ( 161, 126, 137) [     0,  669,    0]
bt15 ( 161, 126, 137) [     0,  795,    0]
bt15 ( 161, 126, 137) [     0,  921,    0]
bt17 ( 154, 142, 179) [     0, 1047,    0]
bt17 ( 142, 154, 179) [     0, 1189,    0]
 bt2 (  46,  45,  41) [     0, 1189,  179]

ct0 ( 242,1360, 245) 25
bt18 ( 154, 149, 148) [     0,    0,    0]
 bt6 (  67,  77,  74) [     0,    0,  148]
bt14 ( 120, 126, 176) [     0,  149,    0]
bt14 ( 120, 126, 176) [   120,  149,    0]
bt15 ( 137, 126, 161) [     0,  275,    0]
bt10 (  95,  90, 136) [   137,  275,    0]
 bt7 (  75,  86,  85) [   137,  275,  136]
bt17 ( 154, 142, 179) [     0,  401,    0]
bt17 ( 154, 142, 179) [     0,  543,    0]
bt14 ( 120, 126, 176) [     0,  685,    0]
 bt5 (  69,  68,  61) [     0,  685,  176]
bt14 ( 120, 126, 176) [   120,  685,    0]
 bt2 (  46,  45,  41) [   120,  685,  176]
 bt2 (  46,  45,  41) [   166,  685,  176]
bt14 ( 120, 126, 176) [     0,  811,    0]
bt14 ( 120, 126, 176) [   120,  811,    0]
bt17 ( 154, 142, 179) [     0,  937,    0]
 bt7 (  86,  75,  85) [   154,  937,    0]
 bt7 (  86,  75,  85) [   154,  937,   85]
bt15 ( 161, 126, 137) [     0, 1079,    0]
bt10 ( 136,  90,  95) [     0, 1079,  137]
 bt7 (  75,  86,  85) [   161, 1012,    0]
 bt7 (  75,  86,  85) [   161, 1098,    0]
bt17 ( 154, 142, 179) [     0, 1205,    0]
 bt4 (  61,  54,  60) [     0, 1205,  179]

ct0 ( 242,1360, 245) 41
bt18 ( 154, 149, 148) [     0,    0,    0]
 bt0 (  40,  33,  37) [     0,    0,  148]
 bt0 (  40,  33,  37) [    40,    0,  148]
 bt0 (  40,  33,  37) [    80,    0,  148]
 bt0 (  40,  33,  37) [     0,   33,  148]
 bt0 (  40,  33,  37) [    40,   33,  148]
 bt0 (  40,  33,  37) [    80,   33,  148]
bt18 ( 154, 149, 148) [     0,  149,    0]
bt18 ( 154, 149, 148) [     0,  298,    0]
bt10 (  90, 136,  95) [     0,  298,  148]
 bt7 (  86,  75,  85) [   154,    0,    0]
 bt5 (  61,  69,  68) [   154,    0,   85]
 bt2 (  46,  45,  41) [   154,    0,  153]
bt17 ( 154, 142, 179) [     0,  447,    0]
 bt5 (  69,  68,  61) [     0,  447,  179]
 bt5 (  69,  68,  61) [    69,  447,  179]
 bt5 (  69,  68,  61) [     0,  515,  179]
 bt5 (  69,  68,  61) [    69,  515,  179]
 bt9 (  86, 123, 122) [   154,   75,    0]
 bt9 (  86, 123, 122) [   154,  198,    0]
 bt9 (  86, 123, 122) [   154,  321,    0]
 bt9 (  86, 123, 122) [   154,  444,    0]
 bt8 (  80, 120, 113) [   154,   75,  122]
 bt8 (  80, 120, 113) [   154,  195,  122]
 bt8 (  80, 120, 113) [   154,  315,  122]
bt19 ( 171, 163, 180) [     0,  589,    0]
 bt3 (  49,  55,  55) [     0,  589,  180]
 bt3 (  49,  55,  55) [    49,  589,  180]
 bt3 (  55,  55,  49) [   171,  567,    0]
 bt3 (  55,  55,  49) [   171,  622,    0]
bt19 ( 180, 163, 171) [     0,  752,    0]
 bt3 (  49,  55,  55) [     0,  752,  171]
bt13 ( 116, 116, 162) [     0,  915,    0]
 bt3 (  55,  55,  49) [     0,  915,  162]
bt11 ( 107,  98, 147) [   116,  915,    0]
bt13 ( 116, 162, 116) [     0, 1031,    0]
bt13 ( 116, 162, 116) [     0, 1031,  116]
bt13 ( 116, 162, 116) [   116, 1013,    0]
bt13 ( 116, 162, 116) [   116, 1013,  116]
bt19 ( 180, 163, 171) [     0, 1193,    0]
 bt1 (  50,  44,  49) [     0, 1193,  171]


Cost = 1719161282