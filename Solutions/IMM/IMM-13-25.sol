%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instance: thpack9-13
% Thu Apr  8 12:23:40 2010
% Syntax:
% NumUsedContainers
% IdContainerType (length,width,height) NumBoxes
% IdBox (length,width,height) container [x, y, z]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

25

ct1 (  35,  35,  40) 4
 bt3 (  25,  35,  20) [     0,    0,    0]
 bt3 (  25,  35,  20) [     0,    0,   20]
 bt2 (  10,  10,  15) [    25,    0,    0]
 bt2 (  10,  10,  15) [    25,    0,   15]

ct1 (  35,  35,  40) 3
 bt3 (  20,  35,  25) [     0,    0,    0]
 bt1 (  15,  25,  20) [    20,    0,    0]
 bt1 (  15,  25,  20) [    20,    0,   20]

ct1 (  35,  35,  40) 3
 bt2 (  15,  10,  10) [     0,    0,    0]
 bt3 (  35,  25,  20) [     0,   10,    0]
 bt3 (  35,  25,  20) [     0,   10,   20]

ct1 (  35,  35,  40) 4
 bt2 (  10,  10,  15) [     0,    0,    0]
 bt2 (  15,  10,  10) [    10,    0,    0]
 bt3 (  35,  25,  20) [     0,   10,    0]
 bt3 (  35,  25,  20) [     0,   10,   20]

ct1 (  35,  35,  40) 4
 bt2 (  15,  10,  10) [     0,    0,    0]
 bt2 (  10,  10,  15) [     0,    0,   10]
 bt3 (  35,  25,  20) [     0,   10,    0]
 bt3 (  35,  25,  20) [     0,   10,   20]

ct1 (  35,  35,  40) 2
 bt3 (  35,  25,  20) [     0,    0,    0]
 bt3 (  35,  25,  20) [     0,    0,   20]

ct1 (  35,  35,  40) 6
 bt1 (  25,  15,  20) [     0,    0,    0]
 bt1 (  25,  15,  20) [     0,    0,   20]
 bt2 (  10,  15,  10) [    25,    0,    0]
 bt2 (  10,  10,  15) [    25,    0,   10]
 bt3 (  35,  20,  25) [     0,   15,    0]
 bt1 (  25,  20,  15) [     0,   15,   25]

ct1 (  35,  35,  40) 2
 bt3 (  35,  25,  20) [     0,    0,    0]
 bt3 (  35,  25,  20) [     0,    0,   20]

ct1 (  35,  35,  40) 5
 bt3 (  25,  20,  35) [     0,    0,    0]
 bt2 (  10,  15,  10) [    25,    0,    0]
 bt1 (  25,  15,  20) [     0,   20,    0]
 bt1 (  25,  15,  20) [     0,   20,   20]
 bt2 (  10,  10,  15) [    25,   20,    0]

ct1 (  35,  35,  40) 4
 bt3 (  20,  35,  25) [     0,    0,    0]
 bt1 (  20,  25,  15) [     0,    0,   25]
 bt1 (  15,  25,  20) [    20,    0,    0]
 bt1 (  15,  25,  20) [    20,    0,   20]

ct1 (  35,  35,  40) 4
 bt1 (  25,  15,  20) [     0,    0,    0]
 bt1 (  25,  15,  20) [     0,    0,   20]
 bt2 (  10,  10,  15) [    25,    0,    0]
 bt3 (  25,  20,  35) [     0,   15,    0]

ct1 (  35,  35,  40) 4
 bt2 (  15,  10,  10) [     0,    0,    0]
 bt2 (  15,  10,  10) [     0,    0,   10]
 bt3 (  35,  25,  20) [     0,   10,    0]
 bt3 (  35,  25,  20) [     0,   10,   20]

ct1 (  35,  35,  40) 3
 bt3 (  35,  25,  20) [     0,    0,    0]
 bt3 (  35,  25,  20) [     0,    0,   20]
 bt2 (  15,  10,  10) [     0,   25,    0]

ct1 (  35,  35,  40) 2
 bt3 (  35,  25,  20) [     0,    0,    0]
 bt3 (  35,  25,  20) [     0,    0,   20]

ct1 (  35,  35,  40) 4
 bt1 (  25,  15,  20) [     0,    0,    0]
 bt1 (  25,  15,  20) [     0,    0,   20]
 bt3 (  35,  20,  25) [     0,   15,    0]
 bt2 (  15,  10,  10) [     0,   15,   25]

ct1 (  35,  35,  40) 2
 bt3 (  35,  25,  20) [     0,    0,    0]
 bt3 (  35,  25,  20) [     0,    0,   20]

ct1 (  35,  35,  40) 4
 bt3 (  35,  20,  25) [     0,    0,    0]
 bt1 (  25,  20,  15) [     0,    0,   25]
 bt1 (  25,  15,  20) [     0,   20,    0]
 bt1 (  25,  15,  20) [     0,   20,   20]

ct1 (  35,  35,  40) 2
 bt3 (  25,  35,  20) [     0,    0,    0]
 bt3 (  25,  35,  20) [     0,    0,   20]

ct1 (  35,  35,  40) 4
 bt3 (  35,  25,  20) [     0,    0,    0]
 bt3 (  35,  25,  20) [     0,    0,   20]
 bt2 (  15,  10,  10) [     0,   25,    0]
 bt2 (  15,  10,  10) [     0,   25,   10]

ct1 (  35,  35,  40) 4
 bt3 (  35,  20,  25) [     0,    0,    0]
 bt1 (  25,  20,  15) [     0,    0,   25]
 bt1 (  25,  15,  20) [     0,   20,    0]
 bt1 (  25,  15,  20) [     0,   20,   20]

ct1 (  35,  35,  40) 2
 bt3 (  25,  35,  20) [     0,    0,    0]
 bt3 (  25,  35,  20) [     0,    0,   20]

ct1 (  35,  35,  40) 4
 bt3 (  35,  25,  20) [     0,    0,    0]
 bt3 (  35,  25,  20) [     0,    0,   20]
 bt2 (  10,  10,  15) [     0,   25,    0]
 bt2 (  15,  10,  10) [    10,   25,    0]

ct1 (  35,  35,  40) 4
 bt2 (  10,  10,  15) [     0,    0,    0]
 bt2 (  15,  10,  10) [    10,    0,    0]
 bt3 (  35,  25,  20) [     0,   10,    0]
 bt3 (  35,  25,  20) [     0,   10,   20]

ct1 (  35,  35,  40) 10
 bt3 (  20,  25,  35) [     0,    0,    0]
 bt1 (  15,  25,  20) [    20,    0,    0]
 bt1 (  15,  25,  20) [    20,    0,   20]
 bt2 (  15,  10,  10) [     0,   25,    0]
 bt2 (  15,  10,  10) [     0,   25,   10]
 bt2 (  15,  10,  10) [     0,   25,   20]
 bt2 (  15,  10,  10) [     0,   25,   30]
 bt2 (  15,  10,  10) [    15,   25,    0]
 bt2 (  15,  10,  10) [    15,   25,   10]
 bt2 (  10,  10,  15) [    15,   25,   20]

ct1 (  35,  35,  40) 5
 bt3 (  35,  20,  25) [     0,    0,    0]
 bt1 (  25,  20,  15) [     0,    0,   25]
 bt1 (  25,  15,  20) [     0,   20,    0]
 bt1 (  25,  15,  20) [     0,   20,   20]
 bt2 (  10,  10,  15) [    25,   20,    0]


Cost = 0