%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instance: thpack9-6
% Thu Apr  8 12:23:41 2010
% Syntax:
% NumUsedContainers
% IdContainerType (length,width,height) NumBoxes
% IdBox (length,width,height) container [x, y, z]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

10

ct1 (  20,  15,  25) 10
 bt2 (   5,  10,  15) [     0,    0,    0]
 bt1 (   5,  10,   5) [     0,    0,   15]
 bt3 (  15,   5,  15) [     5,    0,    0]
 bt3 (  15,   5,  15) [     5,    5,    0]
 bt2 (  15,  10,   5) [     5,    0,   15]
 bt1 (   5,  10,   5) [     5,    0,   20]
 bt1 (   5,   5,  10) [     0,   10,    0]
 bt1 (   5,   5,  10) [     0,   10,   10]
 bt3 (  15,   5,  15) [     5,   10,    0]
 bt2 (  15,   5,  10) [     5,   10,   15]

ct1 (  20,  15,  25) 10
 bt2 (  10,  15,   5) [     0,    0,    0]
 bt2 (  10,  15,   5) [     0,    0,    5]
 bt2 (  10,  15,   5) [     0,    0,   10]
 bt2 (  10,  15,   5) [     0,    0,   15]
 bt2 (  10,  15,   5) [     0,    0,   20]
 bt2 (  10,  15,   5) [    10,    0,    0]
 bt2 (  10,  15,   5) [    10,    0,    5]
 bt2 (  10,   5,  15) [    10,    0,   10]
 bt2 (  10,   5,  15) [    10,    5,   10]
 bt2 (  10,   5,  15) [    10,   10,   10]

ct1 (  20,  15,  25) 7
 bt3 (   5,  15,  15) [     0,    0,    0]
 bt1 (   5,  10,   5) [     0,    0,   15]
 bt3 (  15,  15,   5) [     5,    0,    0]
 bt3 (  15,  15,   5) [     5,    0,    5]
 bt3 (  15,  15,   5) [     5,    0,   10]
 bt3 (  15,  15,   5) [     5,    0,   15]
 bt3 (  15,  15,   5) [     5,    0,   20]

ct1 (  20,  15,  25) 14
 bt2 (  10,   5,  15) [     0,    0,    0]
 bt1 (   5,   5,  10) [     0,    0,   15]
 bt1 (   5,   5,  10) [     5,    0,   15]
 bt1 (  10,   5,   5) [    10,    0,    0]
 bt1 (  10,   5,   5) [    10,    0,    5]
 bt1 (  10,   5,   5) [    10,    0,   10]
 bt1 (   5,   5,  10) [    10,    0,   15]
 bt2 (   5,  10,  15) [     0,    5,    0]
 bt1 (   5,   5,  10) [     0,    5,   15]
 bt1 (   5,   5,  10) [     0,   10,   15]
 bt3 (  15,   5,  15) [     5,    5,    0]
 bt3 (  15,   5,  15) [     5,   10,    0]
 bt2 (  15,  10,   5) [     5,    5,   15]
 bt2 (  15,  10,   5) [     5,    5,   20]

ct1 (  20,  15,  25) 8
 bt3 (   5,  15,  15) [     0,    0,    0]
 bt1 (   5,   5,  10) [     0,    0,   15]
 bt1 (   5,   5,  10) [     0,    5,   15]
 bt3 (  15,  15,   5) [     5,    0,    0]
 bt3 (  15,  15,   5) [     5,    0,    5]
 bt3 (  15,   5,  15) [     5,    0,   10]
 bt3 (  15,   5,  15) [     5,    5,   10]
 bt3 (  15,   5,  15) [     5,   10,   10]

ct1 (  20,  15,  25) 13
 bt2 (  15,   5,  10) [     0,    0,    0]
 bt3 (  15,   5,  15) [     0,    0,   10]
 bt3 (  15,   5,  15) [     0,    5,    0]
 bt1 (   5,   5,  10) [     0,    5,   15]
 bt1 (   5,   5,  10) [     5,    5,   15]
 bt1 (   5,   5,  10) [    10,    5,   15]
 bt1 (   5,   5,  10) [    15,    0,    0]
 bt1 (   5,   5,  10) [    15,    5,    0]
 bt1 (   5,  10,   5) [    15,    0,   10]
 bt1 (   5,   5,  10) [    15,    0,   15]
 bt2 (  10,   5,  15) [     0,   10,    0]
 bt2 (  10,   5,  15) [    10,   10,    0]
 bt1 (   5,   5,  10) [    10,   10,   15]

ct1 (  20,  15,  25) 15
 bt2 (  15,   5,  10) [     0,    0,    0]
 bt3 (  15,   5,  15) [     0,    0,   10]
 bt1 (   5,   5,  10) [    15,    0,    0]
 bt1 (   5,   5,  10) [    15,    0,   10]
 bt1 (   5,   5,  10) [     0,    5,    0]
 bt1 (   5,   5,  10) [     0,    5,   10]
 bt2 (  15,   5,  10) [     5,    5,    0]
 bt2 (  15,   5,  10) [     5,    5,   10]
 bt1 (   5,   5,  10) [     0,   10,    0]
 bt1 (   5,   5,  10) [     0,   10,   10]
 bt1 (   5,   5,  10) [     5,   10,    0]
 bt1 (   5,   5,  10) [     5,   10,   10]
 bt2 (  10,   5,  15) [    10,   10,    0]
 bt1 (   5,   5,  10) [    10,   10,   15]
 bt1 (   5,   5,  10) [    15,   10,   15]

ct1 (  20,  15,  25) 9
 bt2 (   5,  15,  10) [     0,    0,    0]
 bt1 (   5,  10,   5) [     0,    0,   10]
 bt1 (   5,  10,   5) [     0,    0,   15]
 bt3 (  15,  15,   5) [     5,    0,    0]
 bt3 (  15,  15,   5) [     5,    0,    5]
 bt2 (  15,   5,  10) [     5,    0,   10]
 bt2 (  15,   5,  10) [     5,    5,   10]
 bt2 (  15,   5,  10) [     5,   10,   10]
 bt3 (  15,  15,   5) [     5,    0,   20]

ct1 (  20,  15,  25) 10
 bt2 (  10,  15,   5) [     0,    0,    0]
 bt2 (  10,  15,   5) [     0,    0,    5]
 bt2 (  10,  15,   5) [     0,    0,   10]
 bt2 (  10,  15,   5) [     0,    0,   15]
 bt2 (  10,  15,   5) [     0,    0,   20]
 bt2 (  10,  15,   5) [    10,    0,    0]
 bt2 (  10,  15,   5) [    10,    0,    5]
 bt2 (  10,   5,  15) [    10,    0,   10]
 bt2 (  10,   5,  15) [    10,    5,   10]
 bt2 (  10,   5,  15) [    10,   10,   10]

ct1 (  20,  15,  25) 7
 bt3 (  15,  15,   5) [     0,    0,    0]
 bt3 (  15,   5,  15) [     0,    0,    5]
 bt3 (  15,   5,  15) [     0,    5,    5]
 bt3 (  15,   5,  15) [     0,   10,    5]
 bt2 (  15,  10,   5) [     0,    0,   20]
 bt2 (   5,  15,  10) [    15,    0,    0]
 bt3 (   5,  15,  15) [    15,    0,   10]


Cost = 0