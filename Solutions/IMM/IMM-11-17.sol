%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instance: thpack9-11
% Thu Apr  8 12:23:40 2010
% Syntax:
% NumUsedContainers
% IdContainerType (length,width,height) NumBoxes
% IdBox (length,width,height) container [x, y, z]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

17

ct1 (  52,  60,  64) 7
 bt2 (  40,  20,  32) [     0,    0,    0]
 bt2 (  40,  20,  32) [     0,    0,   32]
 bt2 (  32,  40,  20) [     0,   20,    0]
 bt2 (  32,  40,  20) [     0,   20,   20]
 bt2 (  32,  40,  20) [     0,   20,   40]
 bt2 (  20,  40,  32) [    32,   20,    0]
 bt2 (  20,  40,  32) [    32,   20,   32]

ct1 (  52,  60,  64) 6
 bt2 (  40,  20,  32) [     0,    0,    0]
 bt2 (  40,  20,  32) [     0,    0,   32]
 bt1 (  24,  36,  28) [     0,   20,    0]
 bt1 (  24,  36,  28) [     0,   20,   28]
 bt1 (  28,  36,  24) [    24,   20,    0]
 bt1 (  24,  36,  28) [    24,   20,   24]

ct1 (  52,  60,  64) 6
 bt1 (  36,  24,  28) [     0,    0,    0]
 bt1 (  36,  24,  28) [     0,    0,   28]
 bt1 (  24,  36,  28) [     0,   24,    0]
 bt1 (  24,  36,  28) [     0,   24,   28]
 bt1 (  28,  36,  24) [    24,   24,    0]
 bt1 (  28,  24,  36) [    24,   24,   24]

ct1 (  52,  60,  64) 6
 bt1 (  36,  24,  28) [     0,    0,    0]
 bt1 (  36,  24,  28) [     0,    0,   28]
 bt1 (  28,  36,  24) [     0,   24,    0]
 bt1 (  28,  24,  36) [     0,   24,   24]
 bt1 (  24,  36,  28) [    28,   24,    0]
 bt1 (  24,  36,  28) [    28,   24,   28]

ct1 (  52,  60,  64) 6
 bt2 (  40,  20,  32) [     0,    0,    0]
 bt2 (  40,  20,  32) [     0,    0,   32]
 bt2 (  20,  40,  32) [     0,   20,    0]
 bt2 (  20,  40,  32) [     0,   20,   32]
 bt1 (  24,  36,  28) [    20,   20,    0]
 bt1 (  24,  36,  28) [    20,   20,   28]

ct1 (  52,  60,  64) 7
 bt2 (  40,  20,  32) [     0,    0,    0]
 bt2 (  40,  20,  32) [     0,    0,   32]
 bt2 (  20,  40,  32) [     0,   20,    0]
 bt2 (  20,  40,  32) [     0,   20,   32]
 bt2 (  32,  20,  40) [    20,   20,    0]
 bt2 (  32,  20,  40) [    20,   40,    0]
 bt1 (  28,  36,  24) [    20,   20,   40]

ct1 (  52,  60,  64) 6
 bt1 (  36,  24,  28) [     0,    0,    0]
 bt1 (  36,  24,  28) [     0,    0,   28]
 bt1 (  24,  36,  28) [     0,   24,    0]
 bt1 (  24,  36,  28) [     0,   24,   28]
 bt1 (  24,  36,  28) [    24,   24,    0]
 bt1 (  24,  28,  36) [    24,   24,   28]

ct1 (  52,  60,  64) 7
 bt2 (  40,  20,  32) [     0,    0,    0]
 bt2 (  40,  20,  32) [     0,    0,   32]
 bt2 (  20,  40,  32) [     0,   20,    0]
 bt2 (  20,  40,  32) [     0,   20,   32]
 bt2 (  32,  20,  40) [    20,   20,    0]
 bt2 (  32,  20,  40) [    20,   40,    0]
 bt1 (  28,  36,  24) [    20,   20,   40]

ct1 (  52,  60,  64) 7
 bt2 (  40,  20,  32) [     0,    0,    0]
 bt2 (  40,  20,  32) [     0,    0,   32]
 bt2 (  20,  40,  32) [     0,   20,    0]
 bt2 (  20,  40,  32) [     0,   20,   32]
 bt2 (  32,  40,  20) [    20,   20,    0]
 bt2 (  32,  40,  20) [    20,   20,   20]
 bt2 (  32,  40,  20) [    20,   20,   40]

ct1 (  52,  60,  64) 7
 bt2 (  40,  20,  32) [     0,    0,    0]
 bt2 (  40,  20,  32) [     0,    0,   32]
 bt2 (  20,  40,  32) [     0,   20,    0]
 bt2 (  20,  40,  32) [     0,   20,   32]
 bt2 (  32,  20,  40) [    20,   20,    0]
 bt2 (  32,  20,  40) [    20,   40,    0]
 bt1 (  28,  36,  24) [    20,   20,   40]

ct1 (  52,  60,  64) 7
 bt2 (  20,  40,  32) [     0,    0,    0]
 bt2 (  20,  40,  32) [     0,    0,   32]
 bt2 (  32,  40,  20) [    20,    0,    0]
 bt2 (  32,  20,  40) [    20,    0,   20]
 bt2 (  32,  20,  40) [    20,   20,   20]
 bt2 (  40,  20,  32) [     0,   40,    0]
 bt2 (  40,  20,  32) [     0,   40,   32]

ct1 (  52,  60,  64) 6
 bt1 (  36,  24,  28) [     0,    0,    0]
 bt1 (  28,  24,  36) [     0,    0,   28]
 bt1 (  24,  36,  28) [     0,   24,    0]
 bt1 (  24,  36,  28) [     0,   24,   28]
 bt1 (  24,  36,  28) [    24,   24,    0]
 bt1 (  24,  36,  28) [    24,   24,   28]

ct1 (  52,  60,  64) 7
 bt2 (  40,  20,  32) [     0,    0,    0]
 bt2 (  40,  20,  32) [     0,    0,   32]
 bt2 (  20,  40,  32) [     0,   20,    0]
 bt2 (  20,  40,  32) [     0,   20,   32]
 bt2 (  32,  20,  40) [    20,   20,    0]
 bt2 (  32,  20,  40) [    20,   40,    0]
 bt1 (  28,  36,  24) [    20,   20,   40]

ct1 (  52,  60,  64) 6
 bt1 (  36,  24,  28) [     0,    0,    0]
 bt1 (  28,  24,  36) [     0,    0,   28]
 bt1 (  28,  36,  24) [     0,   24,    0]
 bt1 (  28,  36,  24) [     0,   24,   24]
 bt1 (  24,  36,  28) [    28,   24,    0]
 bt1 (  24,  28,  36) [    28,   24,   28]

ct1 (  52,  60,  64) 6
 bt2 (  40,  20,  32) [     0,    0,    0]
 bt2 (  40,  20,  32) [     0,    0,   32]
 bt1 (  24,  36,  28) [     0,   20,    0]
 bt1 (  24,  36,  28) [     0,   20,   28]
 bt1 (  24,  36,  28) [    24,   20,    0]
 bt1 (  24,  28,  36) [    24,   20,   28]

ct1 (  52,  60,  64) 7
 bt2 (  40,  20,  32) [     0,    0,    0]
 bt2 (  40,  20,  32) [     0,    0,   32]
 bt2 (  20,  40,  32) [     0,   20,    0]
 bt2 (  20,  40,  32) [     0,   20,   32]
 bt2 (  32,  40,  20) [    20,   20,    0]
 bt2 (  32,  20,  40) [    20,   20,   20]
 bt2 (  32,  20,  40) [    20,   40,   20]

ct1 (  52,  60,  64) 6
 bt1 (  24,  36,  28) [     0,    0,    0]
 bt1 (  24,  36,  28) [     0,    0,   28]
 bt1 (  24,  36,  28) [    24,    0,    0]
 bt1 (  24,  36,  28) [    24,    0,   28]
 bt1 (  36,  24,  28) [     0,   36,    0]
 bt1 (  36,  24,  28) [     0,   36,   28]


Cost = 0