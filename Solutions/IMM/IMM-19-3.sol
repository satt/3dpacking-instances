%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Instance: thpack9-19
% Thu Apr  8 12:23:40 2010
% Syntax:
% NumUsedContainers
% IdContainerType (length,width,height) NumBoxes
% IdBox (length,width,height) container [x, y, z]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

3

ct1 (  20,  15,  29) 11
 bt3 (  12,   9,   6) [     0,    0,    0]
 bt3 (  12,   9,   6) [     0,    0,    6]
 bt3 (  12,   9,   6) [     0,    0,   12]
 bt3 (  12,   9,   6) [     0,    0,   18]
 bt1 (  10,   7,   4) [     0,    0,   24]
 bt3 (   6,   9,  12) [    12,    0,    0]
 bt2 (   3,   5,  11) [    12,    0,   12]
 bt3 (   9,   6,  12) [     0,    9,    0]
 bt3 (   9,   6,  12) [     0,    9,   12]
 bt3 (   9,   6,  12) [     9,    9,    0]
 bt3 (   9,   6,  12) [     9,    9,   12]

ct1 (  20,  15,  29) 10
 bt3 (   9,   6,  12) [     0,    0,    0]
 bt3 (   9,   6,  12) [     0,    0,   12]
 bt3 (   9,   6,  12) [     9,    0,    0]
 bt3 (   9,   6,  12) [     9,    0,   12]
 bt3 (  12,   9,   6) [     0,    6,    0]
 bt3 (  12,   9,   6) [     0,    6,    6]
 bt3 (  12,   9,   6) [     0,    6,   12]
 bt3 (  12,   9,   6) [     0,    6,   18]
 bt3 (   6,   9,  12) [    12,    6,    0]
 bt3 (   6,   9,  12) [    12,    6,   12]

ct1 (  20,  15,  29) 26
 bt2 (   5,   3,  11) [     0,    0,    0]
 bt3 (   6,  12,   9) [     0,    3,    0]
 bt2 (   3,  11,   5) [     0,    3,    9]
 bt2 (   3,  11,   5) [     3,    3,    9]
 bt2 (   3,  11,   5) [     0,    3,   14]
 bt2 (   3,  11,   5) [     3,    3,   14]
 bt2 (   3,  11,   5) [     0,    3,   19]
 bt1 (   4,   7,  10) [     6,    0,    0]
 bt1 (   4,   7,  10) [     6,    7,    0]
 bt2 (   3,  11,   5) [     6,    0,   10]
 bt2 (   3,  11,   5) [     6,    0,   15]
 bt2 (   3,  11,   5) [     6,    0,   20]
 bt1 (  10,   7,   4) [    10,    0,    0]
 bt1 (  10,   7,   4) [    10,    7,    0]
 bt1 (  10,   7,   4) [    10,    0,    4]
 bt1 (  10,   7,   4) [    10,    7,    4]
 bt1 (  10,   7,   4) [    10,    0,    8]
 bt1 (  10,   7,   4) [    10,    7,    8]
 bt1 (  10,   7,   4) [    10,    0,   12]
 bt1 (  10,   7,   4) [    10,    7,   12]
 bt1 (  10,   7,   4) [    10,    0,   16]
 bt1 (  10,   7,   4) [    10,    7,   16]
 bt1 (  10,   7,   4) [    10,    0,   20]
 bt1 (  10,   7,   4) [    10,    7,   20]
 bt2 (   5,  11,   3) [    10,    0,   24]
 bt2 (   5,  11,   3) [    15,    0,   24]


Cost = 0