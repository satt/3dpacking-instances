# The Multi-Drop Multi-Container Loading Problem

This repository contains instances and solutions for the problem defined in *Local search for a multi-drop multi-container loading problem* by Sara Ceschia and Andrea Schaerf, Journal of Heuristics, 19(2):275-294, 2013.

## Instances

Instances are stored in folder [`Instances`](Instances). 

## Solutions

Best solutions are available in the folder [`Solutions`](Solutions).



